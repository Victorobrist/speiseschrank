from django.db import models
from django.core.validators import MinValueValidator
from django.core.exceptions import ValidationError
from django.utils import timezone
from datetime import datetime

class BaseModel(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


"""
class UOM(models.Model):
    name = models.CharField(max_length=200)    
"""


class Category(BaseModel):
    name = models.CharField(max_length=200)
    description = models.TextField()

    def __str__(self):
        return self.name


class Product(BaseModel):
    name = models.TextField()
    price = models.DecimalField(max_digits=9, decimal_places=2, default=0, validators=[MinValueValidator(0)])
    code = models.TextField(unique=True)
    minimum_qty = models.PositiveIntegerField(default=1, validators=[MinValueValidator(1)])
    category = models.ForeignKey(Category, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class PriceHistory(BaseModel):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name="price_history")
    date = models.DateTimeField(auto_now=True)
    price = models.DecimalField(max_digits=9, decimal_places=2, default=0, validators=[MinValueValidator(0)])
    is_discount = models.BooleanField(default=False)


class Supplier(BaseModel):
    name = models.CharField(max_length=250)
    description = models.TextField()

    def __str__(self):
        return self.name


class Pantry(BaseModel):
    date_filled = models.DateField(auto_now_add=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    date_ended = models.DateTimeField(null=True, blank=True)
    quantity = models.PositiveIntegerField(default=1, validators=[MinValueValidator(0)])
    expiration_date = models.DateField(null=True, blank=True)
    # supplier = models.ForeignKey(Supplier, on_delete=models.CASCADE)

    # class Meta:
    #     constraints = [
    #         models.UniqueConstraint(fields=['product', 'date_filled', 'supplier'],
    #                                 name='pantry_product_date_supplier_constraint')
    #     ]

    def clean(self):
        if not self.id:
            if self.date_filled is not None and self.date_filled < timezone.localdate():
                raise ValidationError("The date filled cannot be less than today")

            if self.expiration_date is not None and self.expiration_date < timezone.localdate():
                raise ValidationError("The expiration date cannot be less than today")

        if self.date_ended is not None and self.date_ended.date() < timezone.localdate():
            raise ValidationError("The date ended cannot be less than today")

    def save(self, *args, **kwargs):
        self.full_clean()
        super().save(*args, **kwargs)
        # print("Pantry Entry created")


class PantryHistory(BaseModel):
    date_filled = models.DateField(auto_now_add=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    date_ended = models.DateTimeField(null=True, blank=True)
    quantity = models.PositiveIntegerField(default=1, validators=[MinValueValidator(0)])
    expiration_date = models.DateField(null=True, blank=True)
    # supplier = models.ForeignKey(Supplier, on_delete=models.CASCADE)
    date_updated = models.DateTimeField(null=True, blank=True)


class Purchase(BaseModel):
    date_bought = models.DateField(auto_now_add=True)
    supplier = models.ForeignKey(Supplier, on_delete=models.CASCADE)
    total = models.DecimalField(max_digits=15, decimal_places=2, default=0, validators=[MinValueValidator(0)])

    def clean(self):
        if self.date_bought is not None and self.date_bought < timezone.localdate():
            raise ValidationError("The date filled cannot be less than today")

    def save(self, *args, **kwargs):
        self.full_clean()
        super().save(*args, **kwargs)


class PurchaseLines(BaseModel):
    purchase = models.ForeignKey(Purchase, on_delete=models.CASCADE, related_name="purchase_lines")
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField(default=1, validators=[MinValueValidator(1)])
    price = models.DecimalField(max_digits=9, decimal_places=2, default=0, validators=[MinValueValidator(0)])
    expiration_date = models.DateField(null=True, blank=True)
