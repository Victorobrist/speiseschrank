from django.urls import path
from .views import ProductsViewSet

app_name = 'products'
urlpatterns = [
    path('/', ProductsViewSet, name='list'),
]