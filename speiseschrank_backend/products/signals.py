from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from .models import Purchase, Pantry, PantryHistory, Product, Supplier, PurchaseLines


@receiver(post_save, sender=PurchaseLines)
def create_pantry_entry(sender, instance, created, **kwargs):
    if created:
        data_pantry = {
            "product": Product.objects.get(pk=instance.product_id),
            # "supplier": Supplier.objects.get(pk=instance.supplier_id),
            "quantity": instance.quantity,
            "expiration_date": instance.expiration_date
        }
        Pantry.objects.create(**data_pantry)


@receiver(post_save, sender=Pantry)
def create_pantry_history_entry(sender, instance, created, **kwargs):
    data_pantry_history = {
        "product": Product.objects.get(pk=instance.product_id),
        # "supplier": Supplier.objects.get(pk=instance.supplier_id),
        "quantity": instance.quantity,
        "expiration_date": instance.expiration_date,
        "date_updated": timezone.now()
    }
    PantryHistory.objects.create(**data_pantry_history)



