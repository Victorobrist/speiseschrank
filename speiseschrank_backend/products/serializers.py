from rest_framework import serializers
from .models import Product, Category, PriceHistory, \
    Pantry, Supplier, Purchase, PantryHistory, PurchaseLines


class PriceHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = PriceHistory
        fields = '__all__'


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'


class SupplierSerializer(serializers.ModelSerializer):
    class Meta:
        model = Supplier
        fields = '__all__'


class ProductSerializer(serializers.ModelSerializer):
    price_history = PriceHistorySerializer(many=True, read_only=True)

    class Meta:
        model = Product
        fields = ['id', 'name', 'price', 'code', 'category', 'price_history']


class PantrySerializer(serializers.ModelSerializer):
    # product_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = Pantry
        fields = '__all__'
        # fields = ['id', 'date_filled', 'date_ended', 'product',
        #           'quantity', 'expiration_date', 'empty_list']
        # depth = 1


class PantryHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = PantryHistory
        fields = '__all__'


class PurchaseLinesSerializer(serializers.ModelSerializer):
    class Meta:
        model = PurchaseLines
        fields = '__all__'
        extra_kwargs = {'purchase': {'required': False}}


class PurchaseSerializer(serializers.ModelSerializer):
    purchase_lines_list = PurchaseLinesSerializer(source='purchase_lines', many=True, read_only=True)

    def create(self, validated_data):
        print("Create from serializer")
        return Purchase.objects.create(**validated_data)


    class Meta:
        model = Purchase
        fields = ['id', 'date_bought', 'supplier', 'total', 'purchase_lines_list']
