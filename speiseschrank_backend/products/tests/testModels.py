from django.test import TestCase
from ..models import Product, Category


class TestModels(TestCase):

    def setUp(self):
        self.category1 = Category.objects.create(
            name="Category 1",
            description="Some description of the category 1"
        )
        self.product1 = Product.objects.create(
            title="Product 1",
            price=100.20,
            category=self.category1
        )

    def test_product_created(self):
        self.assertEquals(self.product1.price, 100.20)


