# Generated by Django 3.1.2 on 2020-10-20 21:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0031_purchaselines_price'),
    ]

    operations = [
        migrations.RemoveConstraint(
            model_name='pantry',
            name='pantry_product_date_supplier_constraint',
        ),
        migrations.RemoveField(
            model_name='pantry',
            name='supplier',
        ),
        migrations.RemoveField(
            model_name='pantryhistory',
            name='supplier',
        ),
    ]
