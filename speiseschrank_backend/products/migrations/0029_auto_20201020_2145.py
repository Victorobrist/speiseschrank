# Generated by Django 3.1.2 on 2020-10-20 19:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0028_auto_20201019_1238'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='purchaselines',
            name='product',
        ),
        migrations.AddField(
            model_name='purchaselines',
            name='expiration_date',
            field=models.DateField(blank=True, null=True),
        ),
    ]
