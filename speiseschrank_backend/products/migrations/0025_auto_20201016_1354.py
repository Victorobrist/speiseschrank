# Generated by Django 3.1.2 on 2020-10-16 11:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0024_auto_20201016_1226'),
    ]

    operations = [
        migrations.AlterField(
            model_name='purchase',
            name='date_bought',
            field=models.DateField(auto_now_add=True),
        ),
    ]
