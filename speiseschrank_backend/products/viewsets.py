from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from django.utils import timezone
from .models import Product, Category, PriceHistory, Pantry, Supplier, Purchase, PantryHistory
from .serializers import ProductSerializer, CategorySerializer, \
    PriceHistorySerializer, PantrySerializer, \
    SupplierSerializer, PurchaseSerializer, PantryHistorySerializer, PurchaseLinesSerializer


class ProductsViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    serializer_class = ProductSerializer
    queryset = Product.objects.all()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            # Save product in database
            self.object = serializer.save()
            # print("Product created")

            # Create an entry in Price History
            data_ph = {
                "product": self.object.id,
                "price": self.object.price
            }

            price_history_serializer = PriceHistorySerializer(data=data_ph)
            if price_history_serializer.is_valid():
                # print("Price History Entry added")
                price_history_serializer.save()

            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=True, methods=['patch'])
    def change_price(self, request, pk):
        instance = Product.objects.get(pk=pk)
        serializer = ProductSerializer(instance, data=request.data, partial=True)

        if serializer.is_valid():
            product_obj = serializer.save()

            # Create an entry in Price History
            data_price_history = {
                "product": product_obj.id,
                "price": product_obj.price
            }

            price_history_serializer = PriceHistorySerializer(data=data_price_history)
            if price_history_serializer.is_valid():
                price_history_serializer.save()

            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CategoryViewSet(viewsets.ModelViewSet):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()


class SupplierViewSet(viewsets.ModelViewSet):
    serializer_class = SupplierSerializer
    queryset = Supplier.objects.all()


class PriceHistoryViewSet(viewsets.ModelViewSet):
    serializer_class = PriceHistorySerializer
    queryset = PriceHistory.objects.all()


class PurchaseViewSet(viewsets.ModelViewSet):
    serializer_class = PurchaseSerializer
    queryset = Purchase.objects.all()

    def create(self, request, *args, **kwargs):
        # print("Request Data")
        # print(request.data)
        # d = {
        #     "supplier": request.data['supplier']
        # }

        serializer = self.get_serializer(data=request.data)
        # serializer = PurchaseSerializer(data=d)
        # print("Serializer Data")
        # print(serializer.initial_data)
        # for purchase_line in request.data['purchase_lines_list']:
        #     serializer.initial_data['total'] += purchase_line['price']

        if serializer.is_valid():
            # Save purchase in database
            purchase_obj = serializer.save()
            # print(serializer.validated_data)

            for purchase_line in request.data['purchase_lines_list']:
                purchase_line['purchase'] = purchase_obj.id
                # print(purchase_line)

            # print(request.data['purchase_lines_list'])

            purchase_line_serializer = PurchaseLinesSerializer(data=request.data['purchase_lines_list'], many=True)
            if purchase_line_serializer.is_valid():
                purchase_line_serializer.save()
            # else:
            #     print("Error on Validation Purchase Lines")
            #     print(purchase_line_serializer.errors)

            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        # else:
        #     print("Error on Validation")
        #     print(serializer.errors)
        # return Response("ok", status=status.HTTP_200_OK)


class PantryViewSet(viewsets.ModelViewSet):
    serializer_class = PantrySerializer
    queryset = Pantry.objects.all()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False)
    def empty_list(self, request):
        # list of products with quantity = 0
        products_list = Pantry.objects.filter(quantity=0).values('id', 'product', 'date_ended')
        products_ids = [p['product'] for p in products_list]
        products_name = Product.objects.filter(id__in=products_ids).values('name')
        print(products_name)
        for i in range(len(products_list)):
            products_list[i]['name'] = products_name[i]['name']

        return Response(products_list)

    @action(detail=False)
    def minimum_list(self, request):
        # list of products that reach minimum quantity value
        products = Pantry.objects.filter(quantity__gt=0).values('id', 'quantity')
        products_ids = [p['id'] for p in products]
        products_list = Product.objects.filter(id__in=products_ids).values('id', 'minimum_qty')

        products_minimum_ids = []
        for i in range(len(products)):
            if products[i]['quantity'] == products_list[i]['minimum_qty']:
                products_minimum_ids.append(products[i]['id'])

        products_minimum_list = Product.objects.filter(id__in=products_minimum_ids).values('id', 'name')
        return Response(products_minimum_list)


class PantryHistoryViewSet(viewsets.ModelViewSet):
    serializer_class = PantryHistorySerializer
    queryset = PantryHistory.objects.all()


"""
class PantryMinimumListViewSet(viewsets.ModelViewSet):
    serializer_class = PantryMinimumListSerializer
    queryset = Pantry.objects.all()
    
    @action(detail=False)
    def list_minimum(self, request):
        list_products_min = PantrySerializer()
        print(list_products_min)

        return Response("ok")

    @action(detail=False)
    def list_minimum2(self, request):
        list_products_min = Pantry.objects.filter(quantity=1)
        # print(list_products_min)
        list_products_min = PantrySerializer(list_products_min, many=True)
        return Response(list_products_min.data)
        # return Response("ok")
   


    @list_route()
    def detalle(self, request, qty=None):
        sigla = self.request.query_params.get('sigla', None)
        trecho = Trechos.objects.get(sigla=sigla)
        serializer = self.get_serializer(trecho)
        return Response(serializer.data)
"""
