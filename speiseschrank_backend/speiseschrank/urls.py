"""speiseschrank URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework.routers import DefaultRouter
from products.viewsets import ProductsViewSet, CategoryViewSet, PantryViewSet, \
    SupplierViewSet, PurchaseViewSet, PantryHistoryViewSet

router = DefaultRouter()
router.register(r'products', ProductsViewSet)
router.register(r'category', CategoryViewSet)
router.register(r'pantry', PantryViewSet)
router.register(r'supplier', SupplierViewSet)
router.register(r'purchase', PurchaseViewSet)
router.register(r'pantry_history', PantryHistoryViewSet)

urlpatterns = [
    # path('products/', include('products.urls')),
    path('api/', include(router.urls)),
    path('api/auth/', include('djoser.urls.authtoken')),
    path('admin/', admin.site.urls),
]
